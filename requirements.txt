Scrapy==0.17.0
Twisted==12.2.0
argparse==1.2.1
lxml==3.0.1
pyOpenSSL==0.13
w3lib==1.2
wsgiref==0.1.2
zope.interface==4.0.1
